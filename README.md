# DiPET NIDS dev

Development repo for the network intrusion detection system (NIDS) use case of the Distributed Stream processing on Fog and Esge Systems via Transprecise Computing (DiPET) project


# Roadmap
* We first explore the potential of NetFlow.
* Next step might be sflow.

# NetFlow - Proposal

```mermaid
graph LR
    Alt[pmacctd].->B
    A[eXo <br> router] -->|nf records<br>v9, UDP 2100| B[nfacctd]
    B -->|topic: nfraw| C[kafka]
    C --> D[flink]
    D -->|eXosrcIP, bytes, xor ,eXodstIP, bytes,<br>maybe accumulated by time periods <br> topic: nfexoin, nfexoout| E[eXo<br><br>prometehus<br>/ kafka]
    D -->|Crypto-PAn on eXpIPs<br>w. temporary encryp key<br>kafka topic: nfcryp| F[UPC<br><br>kafka]
    style A fill:#fff,stroke-width:1px
    style Alt fill:#fff,stroke-width:1px
```

1. nfacctd receives netflow records from the eXo router (or from pmacctd in dev) and (only) pushes them into kafka
2. flink gets raw nf records from kafka. for each record:
    2.1 eXo
    2.2 UPC

### Considerations
* nfacctd can also do accounting, so it could do eXo traffic accounting and then push it to kafka and then to prometheus (nfacctd has no plugin for prometheus)
    * discarted for the time being. we want first to see the performance with flink (where we are already parsing each record for UPC)


# ISP info
## Network
AS208253

45.150.184.0/22

2a0f:de00::/29
```
$ ipcalc 45.150.184.0/22
Address:   45.150.184.0         00101101.10010110.101110 00.00000000
Netmask:   255.255.252.0 = 22   11111111.11111111.111111 00.00000000
Wildcard:  0.0.3.255            00000000.00000000.000000 11.11111111
=>
Network:   45.150.184.0/22      00101101.10010110.101110 00.00000000
HostMin:   45.150.184.1         00101101.10010110.101110 00.00000001
HostMax:   45.150.187.254       00101101.10010110.101110 11.11111110
Broadcast: 45.150.187.255       00101101.10010110.101110 11.11111111
Hosts/Net: 1022
```
```
$ subnetcalc 2a0f:de00::/29
Address       = 2a0f:de00::
                   2a0f = 00101010 00001111
                   de00 = 11011110 00000000
                   0000 = 00000000 00000000
                   0000 = 00000000 00000000
                   0000 = 00000000 00000000
                   0000 = 00000000 00000000
                   0000 = 00000000 00000000
                   0000 = 00000000 00000000
Network       = 2a0f:de00:: / 29
Netmask       = ffff:fff8::
Wildcard Mask = 0:7:ffff:ffff:ffff:ffff:ffff:ffff
Hosts Bits    = 99
Max. Hosts    = 2^99 - 1
Host Range    = { 2a0f:de00::1 - 2a0f:de07:ffff:ffff:ffff:ffff:ffff:ffff }
Properties    =
   - 2a0f:de00:: is a NETWORK address
   - Global Unicast Properties:
      + Interface ID = 0000:0000:0000:0000
      + Sol. Node MC = ff02::1:ff00:0000
      + 6to4 address = 222.0.0.0
GeoIP Country = Spain (ES)
DNS Hostname  = 2a0f-de00-0000-0000-0000-0000-0000-0000.ipv6.at.exonet.cat
```
## Setup
* Currently the ISP router is in "default route" mode, thus, all parameters related to BGP are not visible/relevant

# Bibliography

### NetFlow IPFIX technologies
(2014) R. Hofstede et al., **Flow Monitoring Explained: From Packet Capture to Data Analysis with NetFlow and IPFIX** https://research.utwente.nl/files/6519120/tutorial.pdf *Introduction tutorial*

(2011) CISCO **NetFlow Version 9 Flow-Record Format** https://www.cisco.com/en/US/technologies/tk648/tk362/technologies_white_paper09186a00800a3db9.html *Reference manual*

### Attacks
(2018) NetFlow Logic **NetFlow-based DDoS Detection** https://www.netflowlogic.com/wp-content/uploads/2018/05/NetFlow-based_DDoS_Detection_Solution_Guide_2.5.1.pdf *Interesting list of attacks and parameters for detection*


### IDS Deterministic
(2013) Galtsev, A. & Sukhov, A. **Detecting network attacks at flow level** https://www.researchgate.net/profile/A_Sukhov2/publication/274229148_Detecting_network_attacks_at_flow_level/links/59f711250f7e9b553ebd5399/Detecting-network-attacks-at-flow-level.pdf *Simple algorithm for DoS and DDoS*

### IDS Machine Learning

(2015) A. Buczak et al., **A Survey of Data Mining and Machine Learning Methods for Cyber Security Intrusion Detection** http://www2.cs.uh.edu/~acl/cs6397/Presentation/2016-IEEE-A%20survey%20of%20DM%20and%20ML%20methods%20for%20cyber%20security%20ID.pdf *Intrusion detection: review of techniques*

### 2020/12/07 Usecase meeting
#### Signature approach
##### Approach 1: pre-defined patterns that indicate intrusions
##### Approach 2: deep learning based: labeled data sets; neural networks identify patterns (e.g. Lucid)
(2020) R. Doriguzzi-Corin et al. **Lucid: A Practical, Lightweight Deep Learning Solution for DDoS Attack Detection** https://ieeexplore.ieee.org/document/8984222

#### Anomaly approach
##### Autoencoder (UPC)
##### Netflows as graph streams
(2019) B. Siddharth et al. **MIDAS: Microcluster-Based Detector of Anomalies in Edge Streams** https://www.comp.nus.edu.sg/~sbhatia/assets/pdf/midas.pdf

(2016) E. Manzoor et alt. **Fast Memory-efficient Anomaly Detection in Streaming Heterogeneous Graphs** https://www.kdd.org/kdd2016/papers/files/rfp0693-manzoorA.pdf
