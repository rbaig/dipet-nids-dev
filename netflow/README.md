# Usage
## Installation

### Prepare development and production environments

```
git clone https://gitlab.com/rbaig/dipet-nids-dev.git
cd dipet-nids-dev/netflow
./gen_templates.sh
```

`./gen_templates.sh` script copies template directory taking in account the lines that
change between the different development and production environments:

The rest of this document refers to `yourenv` as the environment chosen for your use case, hence, `yourenv` should be one of those:

- `production`: to receive real netflow records
- `development`: to receive specific netflow records generated from your PC
- `nflowsim`: to receive simulated netflow records generated from your PC

## Up

* Add `--build` option if local changes have been made (Dockerfile, .py, etc.)

### Foreground

```
docker-compose --file netflow/yourenv/docker-compose.yml up
```

### Background

```
docker-compose --file netflow/yourenv/docker-compose.yml up -d
```
* View output from cotaniners: `docker-compose --file netflow/yourenv/docker-compose.yml logs -f`

## Down

* `nfraw` topic must be made empty before next run otherwise pre-existing messages are likely to lead to undesired results (e.g. duplicated messages in messages `nfanon`)
* We do it by removing `data_kafka` volume (kafka native methods are more elegant and complex)

### Foreground

```
Ctrl+C
```

### All volumes
```
docker-compose --file netflow/yourenv/docker-compose.yml down -v
```

### Only `data_kafka` volume
```
docker-compose --file netflow/yourenv/docker-compose.yml stop
docker-compose --file netflow/yourenv/docker-compose.yml rm -f kafka
docker volume rm kafka_data
```

## Restart
* **Don't forget to make `nfraw` topic empty before**

## Development: records generation
```
docker-compose --file netflow/yourenv/docker-compose.yml exec pmacctd ping -c 3 -s 4439 1.1.1.1
```
```
docker-compose --file netflow/yourenv/docker-compose.yml exec pmacctd wget https://upc.edu
```
* If IPv6 enabled (https://medium.com/@skleeschulte/how-to-enable-ipv6-for-docker-containers-on-ubuntu-18-04-c68394a219a2):
```
docker-compose --file netflow/yourenv/docker-compose.yml exec pmacctd ping -6 google.com
```

# Datasets
## Generation
### All
```
FILE="nftraces_${EPOCHSECONDS}.json"
```

### Development
```
docker-compose --file netflow/yourenv/docker-compose.yml logs -f kafkacat-nfraw kafkacat-nfano > ${FILE}
```

### Production
* Must be done twice :(
* Set a timeout prepending with `timeout <seconds>` (like in `timeout 5 sleep 100`)
```
docker-compose --file netflow/yourenv/docker-compose.yml up -d kafkacat-nfraw kafkacat-nfano
docker-compose --file netflow/yourenv/docker-compose.yml logs -f kafkacat-nfraw kafkacat-nfano > ${FILE}
```
### All
```
grep kafkacat-nfano ${FILE} > ${FILE%.*}_nfano.json

FILE=${FILE%.*}_nfano.json

## Ensure that no real IPs are leaked
grep -E '\"45\.150\.\|\"10\.2\.\|\"192\.168\.|\"172\.19\.' ${FILE}

## Remove everything outside '{' '}' and remove any line not starting with '{' and ending with '}'
sed -i 's/^.*\({.*}\).*$/\1/; /^\({.*}\)/!d' ${FILE}
```
Add heading '[' and tailing ']' if needed
```
if [ "$(head -1 ${FILE})" != "[" ]
then
    echo "[" > ${FILE}.tmp
    cat ${FILE} >> ${FILE}.tmp
else
    cat ${FILE} > ${FILE}.tmp
fi

if [ "$(tail -1 ${FILE}.tmp)" != "]" ]
then
    echo "]" >> ${FILE}.tmp
fi

mv ${FILE}.tmp ${FILE}
```

## To .cvs
```
## Add ',' after '}' except for the last record
sed -n '1h;2,$H;${x;s/}\n{/},\n{/g;p;}' ${FILE} > ${FILE}.tmp

## 'label' is a reserved word in 'jq'
sed -i 's/label/LABEL/g' ${FILE}.tmp

jq -r 'map({
event_type, tag, tag2, LABEL, class, mac_src, mac_dst, vlan, cos, etype, as_src, as_dst, comms, ecomms, lcomms,
as_path, local_pref, med, roa_dst, peer_as_src, peer_as_dst, peer_ip_src, peer_ip_dst,  comms_src, ecomms_src, lcomms_src,
as_path_src, local_pref_src, med_src, roa_src, iface_in, iface_out, mpls_vpn_rd, mpls_pw_id, ip_src, net_src, ip_dst, net_dst,
mask_src, mask_dst, port_src, port_dst, tcp_flags, ip_proto, tos, sampling_rate, sampling_direction,
post_nat_ip_src, post_nat_ip_dst, post_nat_port_src, post_nat_port_dst, nat_event, mpls_label_top, mpls_label_bottom, mpls_stack_depth,
tunnel_mac_src, tunnel_mac_dst, tunnel_ip_src, tunnel_ip_dst, tunnel_ip_proto, tunnel_tos, tunnel_port_src, tunnel_port_dst, vxlan,
timestamp_start, timestamp_end, timestamp_arrival, export_proto_seqno, export_proto_version, export_proto_sysid,
stamp_inserted, stamp_updated, flows, packets, bytes, writer_id
}) | (first | keys_unsorted) as $keys | map([to_entries[] | .value]) as $rows | $keys,$rows[] | @csv' ${FILE}.tmp > ${FILE%.*}.cvs

rm ${FILE}.tmp
```
* Info https://e.printstacktrace.blog/how-to-convert-json-to-csv-from-the-command-line/

# Records

## Parameters

| Name | Standard | Description | Values prod | Values dev | Understood | Relevant |
| ---- | -------- | ----------- |------------ | ---------- | ---------- | -------- |
| event_type | -     | -     |-------- | ---------- |--------- | --------- |
| tag | - | - | - | - |- | - |
| tag2 | - | - | - | - |- | - |
| LABEL | - | - | - | - |- | - |
| class | - | - | - | - |- | - |
| mac_src | - | - | - | - |- | - |
| mac_dst | - | - | - | - |- | - |
| vlan | - | - | - | - |- | - |
| cos | - | - | - | - |- | - |
| etype | - | - | - | - |- | - |
| as_src | - | - | - | - |- | - |
| as_dst | - | - | - | - |- | - |
| comms | - | - | - | - |- | - |
| ecomms | - | - | - | - |- | - |
| lcomms | - | - | - | - |- | - |
| as_path | - | - | - | - |- | - |
| local_pref | - | - | - | - |- | - |
| med | - | - | - | - |- | - |
| roa_dst | - | - | - | - |- | - |
| peer_as_src | - | - | - | - |- | - |
| peer_as_dst | - | - | - | - |- | - |
| peer_ip_src | - | - | - | - |- | - |
| peer_ip_dst | - | - | - | - |- | - |
| comms_src | - | - | - | - |- | - |
| ecomms_src | - | - | - | - |- | - |
| lcomms_src | - | - | - | - |- | - |
| as_path_src | - | - | - | - |- | - |
| local_pref_src | - | - | - | - |- | - |
| med_src | - | - | - | - |- | - |
| roa_src | - | - | - | - |- | - |
| iface_in | - | - | - | - |- | - |
| iface_out | - | - | - | - |- | - |
| mpls_vpn_rd | - | - | - | - |- | - |
| mpls_pw_id | - | - | - | - |- | - |
| ip_src | - | - | - | - |- | - |
| net_src | - | - | - | - |- | - |
| ip_dst | - | - | - | - |- | - |
| net_dst | - | - | - | - |- | - |
| mask_src | - | - | - | - |- | - |
| mask_dst | - | - | - | - |- | - |
| port_src | - | - | - | - |- | - |
| port_dst | - | - | - | - |- | - |
| tcp_flags | - | - | - | - |- | - |
| ip_proto | - | - | - | - |- | - |
| tos | - | - | - | - |- | - |
| sampling_rate | - | - | - | - |- | - |
| sampling_direction | - | - | - | - |- | - |
| post_nat_ip_src | - | - | - | - |- | - |
| post_nat_ip_dst | - | - | - | - |- | - |
| post_nat_port_src | - | - | - | - |- | - |
| post_nat_port_dst | - | - | - | - |- | - |
| nat_event | - | - | - | - |- | - |
| mpls_label_top | - | - | - | - |- | - |
| mpls_label_bottom | - | - | - | - |- | - |
| mpls_stack_depth | - | - | - | - |- | - |
| tunnel_mac_src | - | - | - | - |- | - |
| tunnel_mac_dst | - | - | - | - |- | - |
| tunnel_ip_src | - | - | - | - |- | - |
| tunnel_ip_dst | - | - | - | - |- | - |
| tunnel_ip_proto | - | - | - | - |- | - |
| tunnel_tos | - | - | - | - |- | - |
| tunnel_port_src | - | - | - | - |- | - |
| tunnel_port_dst | - | - | - | - |- | - |
| vxlan | - | - | - | - |- | - |
| timestamp_start | - | - | - | - |- | - |
| timestamp_end | - | - | - | - |- | - |
| timestamp_arrival | - | - | - | - |- | - |
| export_proto_seqno | - | - | - | - |- | - |
| export_proto_version | - | - | - | - |- | - |
| export_proto_sysid | - | - | - | - |- | - |
| stamp_inserted | - | - | - | - |- | - |
| stamp_updated | - | - | - | - |- | - |
| flows | - | - | - | - |- | - |
| packets | - | - | - | - |- | - |
| bytes | - | - | - | - |- | - |
| writer_id | - | - | - | - |- | - |

## Samples
### Develpoment

### eXo
```
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "165.146.247.19", "net_src": "0.0.0.0", "ip_dst": "215.249.77.227", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 61907, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012180", "export_proto_seqno": 2340363597, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 1, "bytes": 1209, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "174.219.149.36", "net_src": "0.0.0.0", "ip_dst": "215.249.77.24", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 64664, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012181", "export_proto_seqno": 2340363597, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 2, "bytes": 180, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "173.119.175.229", "net_src": "0.0.0.0", "ip_dst": "215.249.77.227", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 63766, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012182", "export_proto_seqno": 2340363597, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 1, "bytes": 142, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "172.33.244.175", "net_src": "0.0.0.0", "ip_dst": "215.249.77.227", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 58140, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012183", "export_proto_seqno": 2340363597, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 4, "bytes": 2740, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "205.143.119.20", "net_src": "0.0.0.0", "ip_dst": "215.249.77.227", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 54001, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012184", "export_proto_seqno": 2340363597, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 2, "bytes": 2476, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "172.47.46.189", "net_src": "0.0.0.0", "ip_dst": "215.249.77.24", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 62254, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012185", "export_proto_seqno": 2340363597, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 3, "bytes": 2447, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "172.40.202.208", "net_src": "0.0.0.0", "ip_dst": "215.249.77.24", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 54156, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012192", "export_proto_seqno": 2340363598, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 1, "bytes": 1166, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "221.121.91.45", "net_src": "0.0.0.0", "ip_dst": "215.249.79.155", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 8801, "port_dst": 56347, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012192", "export_proto_seqno": 2340363598, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 1, "bytes": 326, "writer_id": "default_kafka/100"}
{"event_type": "purge", "tag": 0, "tag2": 0, "LABEL": "", "class": "unknown", "mac_src": "04:25:c5:e7:72:77", "mac_dst": "6a:3e:5c:a3:93:02", "vlan": 0, "cos": 0, "etype": "800", "as_src": 0, "as_dst": 0, "comms": "", "ecomms": "", "lcomms": "", "as_path": "", "local_pref": 0, "med": 0, "roa_dst": "u", "peer_as_src": 0, "peer_as_dst": 0, "peer_ip_src": "105.224.63.126", "peer_ip_dst": "", "comms_src": "", "ecomms_src": "", "lcomms_src": "", "as_path_src": "", "local_pref_src": 0, "med_src": 0, "roa_src": "u", "iface_in": 5, "iface_out": 0, "mpls_vpn_rd": "0:0:0", "mpls_pw_id": 0, "ip_src": "172.53.142.91", "net_src": "0.0.0.0", "ip_dst": "215.249.77.227", "net_dst": "0.0.0.0", "mask_src": 0, "mask_dst": 0, "port_src": 60648, "port_dst": 10000, "tcp_flags": "0", "ip_proto": "udp", "tos": 0, "sampling_rate": 0, "sampling_direction": "i", "post_nat_ip_src": "", "post_nat_ip_dst": "", "post_nat_port_src": 0, "post_nat_port_dst": 0, "nat_event": 0, "mpls_LABEL_top": 0, "mpls_LABEL_bottom": 0, "mpls_stack_depth": 0, "tunnel_mac_src": "00:00:00:00:00:00", "tunnel_mac_dst": "00:00:00:00:00:00", "tunnel_ip_src": "", "tunnel_ip_dst": "", "tunnel_ip_proto": "0", "tunnel_tos": 0, "tunnel_port_src": 0, "tunnel_port_dst": 0, "vxlan": 0, "timestamp_start": "2020-12-15 17:10:01.000000", "timestamp_end": "2020-12-15 17:10:01.000000", "timestamp_arrival": "2020-12-15 17:10:47.012194", "export_proto_seqno": 2340363598, "export_proto_version": 9, "export_proto_sysid": 59136, "stamp_inserted": "2020-12-15 17:10:00", "stamp_updated": "2020-12-15 17:10:51", "flows": 1, "packets": 1, "bytes": 74, "writer_id": "default_kafka/100"}
```

# WiP
## Apache flink
### Example
Terminal 1:
```
docker-compose --file netflow/yourenv/docker-compose.yml logs --follow flink-jobmanager
```
Terminal 2:
```
docker-compose --file netflow/yourenv/docker-compose.yml logs --follow flink-taskmanager
```
Terminal 3:
```
wget https://archive.apache.org/dist/flink/flink-1.11.1/flink-1.11.1-bin-scala_2.12.tgz
tar xf flink-1.11.1-bin-scala_2.12.tgz
```
```
docker container run \
    --rm \
    -d \
    --name=java-flink-topspeedwindowing \
    -p 8082:8081 \
    --network NFinternal  \
    -v $(pwd)/flink-1.11.1:/opt/flink \
    -w /opt/flink \
    openjdk:8 ./bin/flink run -m flink-jobmanager:8081 ./examples/streaming/TopSpeedWindowing.jar
```
```
docker container exec \
    -w /opt/flink \
    java-flink-topspeedwindowing ./bin/flink list -m flink-jobmanager:8081
```
```
docker container exec \
    -w /opt/flink \
    java-flink-topspeedwindowing ./bin/flink cancel -m flink-jobmanager:8081 6c4572f249f9ba3552d85f0c80a62cee
```



# TODO
nfacctd container stops if daemonized

alternative to nfacctd: https://github.com/cloudflare/goflow

# Retrieve kafka outside docker-compose

use nflowsim docker compose template, install dependency `pip3 install kafka-python` and this python script (don't put name `kafka.py`)

```
from kafka import KafkaConsumer

bootstrap_servers = ['127.0.0.1:9092']
topic_name='nfraw'

consumer = KafkaConsumer (topic_name, bootstrap_servers = bootstrap_servers)

for msg in consumer:
  print (msg)
```
