#!/bin/sh -e

# reproduce dashboards generation
#   preserving original references and authors

prometheus='Prometheus'

wget -O docker_and_os_metrics.json https://grafana.com/api/dashboards/10566/revisions/1/download
sed -i "s/\${DS_PROMETHEUS_INFRASTRUCTURE}/$prometheus/g" docker_and_os_metrics.json
sed -i "s/\${VAR_DATACENTER}/LocalhostDC/g" docker_and_os_metrics.json
# TODO contains alerts: fix or clean

wget -O docker_and_system_monitoring.json https://grafana.com/api/dashboards/893/revisions/1/download
sed -i "s/\${DS_PROMETHEUS}/$prometheus/g" docker_and_system_monitoring.json
# TODO contains alerts: fix or clean

wget -O docker_and_system_monitoring_2.json https://grafana.com/api/dashboards/13496/revisions/1/download
sed -i "s/\${DS_PROMETHEUS}/$prometheus/g" docker_and_system_monitoring_2.json
# TODO contains alerts: fix or clean

wget -O docker_containers.json https://raw.githubusercontent.com/stefanprodan/dockprom/master/grafana/provisioning/dashboards/docker_containers.json

#wget -O kafka.json https://grafana.com/api/dashboards/14506/revisions/1/download
# dashboard is adapted


echo -------------------------
