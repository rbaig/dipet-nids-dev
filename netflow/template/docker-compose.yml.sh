#!/bin/sh -e

DC_CONFIG="$(cat <<EOF
version: "3.5"

services:
  zookeeper:
    ## https://github.com/31z4/zookeeper-docker/blob/master/3.6.1/Dockerfile
    ## (official) https://hub.docker.com/_/zookeeper?tab=description
    image: zookeeper:3.6.1
    container_name: ${TEMPLATE}_zookeeper
    ## expose suffices
    expose:
      - "2181"
    logging:
        driver: "json-file"
        options:
            max-file: "5"
            max-size: "50m"
    volumes:
      - type: volume
        source: data_zk_conf
        target: /conf
      - type: volume
        source: data_zk_data
        target: /data
      ## TODO creates a log file each time it is started /var/lib/docker/volumes/data_zk_datalog/_data/version-2/
      - type: volume
        source: data_zk_datalog
        target: /datalog
      - type: volume
        source: data_zk_logs
        target: /logs
      - /etc/localtime:/etc/localtime:ro
    networks:
      - NFinternal

  kafka:
    ## https://github.com/wurstmeister/kafka-docker
    ## https://hub.docker.com/r/wurstmeister/kafka/dockerfile
    image: wurstmeister/kafka
    container_name: ${TEMPLATE}_kafka
    ## must be restarted because at the startup exits because zookeeper is not ready
    restart: unless-stopped
    depends_on:
      - zookeeper
    ## expose suffices
$( if [ $TEMPLATE = 'nflowsim' ] || [ $TEMPLATE = 'development' ]; then cat <<EOF2
    ports:
      - "9092:9092"
      - "1099:1099"
EOF2
else cat <<EOF2
    expose:
      - 19092
EOF2
fi
)
    logging:
        driver: "json-file"
        options:
            max-file: "5"
            max-size: "50m"
    environment:
      ## from docker host command line:
      ##   kafkacat -b 127.0.0.1 -C -t nfraw -o -1000
      ##   kafkacat -b 127.0.0.1:9092 -C -t nfraw -o -1000
      ##   docker run --rm -it --network=NFinternal edenhill/kafkacat:1.6.0 -b kafka:19092 -C -t nfraw
      KAFKA_ADVERTISED_LISTENERS: INSIDE://:19092,OUTSIDE://localhost:9092
      KAFKA_LISTENERS: INSIDE://:19092,OUTSIDE://:9092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: INSIDE:PLAINTEXT,OUTSIDE:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: INSIDE
      ## https://github.com/wurstmeister/kafka-docker/blob/master/create-topics.sh
      ##   Expected format "name:partitions:replicas:cleanup.policy"
      ## TODO 3 partitions needed?
      KAFKA_CREATE_TOPICS: "nfraw:1:1,nfano:1:1"
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_AUTO_CREATE_TOPICS_ENABLE: 'false'
      KAFKA_ADVERTISED_HOST_NAME: kafka
      JMX_PORT: 1099
      KAFKA_JMX_OPTS: "-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=kafka -Dcom.sun.management.jmxremote.rmi.port=1099"
$( [ $TEMPLATE = 'production' ] && cat <<EOF2
      KAFKA_LOG_RETENTION_HOURS: 1
EOF2
)
      ## needs zk /data (data_zk_data) to be preserved (to use the same cluster ID)
      KAFKA_LOG_DIRS: /kafka/kafka-logs-1
      KAFKA_BROKER_ID: 1
    volumes:
      - type: volume
        source: data_kafka
        target: /kafka
      ## is docker.sock needed? https://stackoverflow.com/questions/35110146/can-anyone-explain-docker-sock/35110344
      - /var/run/docker.sock:/var/run/docker.sock
      - /etc/localtime:/etc/localtime:ro
    networks:
      - NFinternal

  # thanks https://github.com/wurstmeister/kafka-docker/issues/652#issue-860845558
  kafka-web-gui:
    image: tchiotludo/akhq
    container_name: ${TEMPLATE}_kafka_webgui
    ports:
      - "8081:8080"
    environment:
      AKHQ_CONFIGURATION: |
        akhq:
          connections:
            docker-kafka-server:
              properties:
                bootstrap.servers: "kafka:19092"
    networks:
      - NFinternal

  nfipano:
    build: nfipano_py/
    image: nfipano_py
    container_name: ${TEMPLATE}_nfipano
    restart: unless-stopped
    logging:
        driver: "json-file"
        options:
            max-file: "5"
            max-size: "50m"
    volumes:
      - ./nfipano_py/nfIPanonymizerKafka.py:/usr/src/myapp/nfIPanonymizerKafka.py:ro
      - /etc/localtime:/etc/localtime:ro
    networks:
      - NFinternal
    command: [ "python", "/usr/src/myapp/nfIPanonymizerKafka.py" ]

  kafkacat-nfraw:
    image: edenhill/kafkacat:1.6.0
    ## must be restarted because at the startup exits because kafka broker is not ready
    container_name: ${TEMPLATE}_kafkacat-nfraw
$( [ $TEMPLATE = 'development' ] && cat <<EOF2
    restart: unless-stopped
EOF2
)
    logging:
        driver: "json-file"
        options:
            max-file: "5"
            max-size: "50m"
    volumes:
      - /etc/localtime:/etc/localtime:ro
    networks:
      - NFinternal
    command: [ "-b", "kafka:19092", "-C", "-t", "nfraw", "-o", "-1000" ]

  kafkacat-nfano:
    image: edenhill/kafkacat:1.6.0
    ## must be restarted because at the startup exits because kafka borker is not ready
    container_name: ${TEMPLATE}_kafkacat-nfano
$( [ $TEMPLATE = 'development' ] && cat <<EOF2
    restart: unless-stopped
EOF2
)
    logging:
        driver: "json-file"
        options:
            max-file: "5"
            max-size: "50m"
    volumes:
      - /etc/localtime:/etc/localtime:ro
    networks:
      - NFinternal
    command: [ "-b", "kafka:19092", "-C", "-t", "nfano", "-o", "-1000" ]

  # thanks https://github.com/wurstmeister/kafka-docker/blob/1c54372e408b07c8d7476c4003858684d8370129/test/scenarios/jmx/docker-compose.yml
  jmxexporter:
    image: sscaling/jmx-prometheus-exporter
    container_name: ${TEMPLATE}_jmxexporter
    ports:
      - "5556:5556"
    environment:
      SERVICE_PORT: 5556
    volumes:
      - \$PWD/jmxexporter.yml:/opt/jmx_exporter/config.yml
    networks:
      - NFinternal

  nfacctd:
    ## https://hub.docker.com/r/pmacct/nfacctd
    ## https://github.com/pmacct/pmacct/blob/master/docker/nfacctd/Dockerfile
    image: pmacct/nfacctd:v1.7.5
    container_name: ${TEMPLATE}_nfacctd
    depends_on:
      - kafka
    restart: unless-stopped
    ports:
      - "2100:2100/udp"
    logging:
        driver: "json-file"
        options:
            max-file: "5"
            max-size: "50m"
    volumes:
      - ./pmacct/etc/nfacctd_kafka.conf:/etc/pmacct/nfacctd.conf:ro
      - /etc/localtime:/etc/localtime:ro
    networks:
      - internet
      - NFinternal
    ## for DEBUG use "debug: true" directive in config (just tested with "tee" plugin)
    ##   if cmd with "-d" -> config file must be explicitly declared
    ##    nfacctd_1  | INFO ( default/core ): Reading configuration from cmdline.
#    command: [ "-d", "-f", "/etc/pmacct/nfacctd.conf" ]

$( [ $TEMPLATE = 'development' ] && cat <<EOF2
  pmacctd:
    ## https://hub.docker.com/r/pmacct/pmacctd
    ## https://github.com/pmacct/pmacct/blob/master/docker/pmacctd/Dockerfile
    image: pmacct/pmacctd:v1.7.5
    container_name: ${TEMPLATE}_pmacctd
#    depends_on:
#      - nfacctd
    restart: unless-stopped
    logging:
        driver: "json-file"
        options:
            max-file: "5"
            max-size: "50m"
    volumes:
      - ./pmacct/etc/pmacctd.conf:/etc/pmacct/pmacctd.conf:ro
      - /etc/localtime:/etc/localtime:ro
    networks:
      - internet
    ## for DEBUG use "debug: true" directive in config (just tested with "tee" plugin)
    ##   because cmd with "-d" prevents plugins from being loaded (do not use 'command: [ "-d" ]')
    ##     pmacctd_1      | WARN: [cmdline] No plugin has been activated; defaulting to in-memory table.
EOF2
)

$( [ $TEMPLATE = 'nflowsim' ] && cat <<EOF2
  # https://github.com/nerdalert/nflow-generator
  nflow-generator:
    image: networkstatic/nflow-generator
    container_name: ${TEMPLATE}_nflowsim
    restart: unless-stopped
    command: [ "./nflow-generator", "-t", "nfacctd", "-p", "2100" ]
    networks:
      - internet
EOF2
)

  # src https://prometheus.io/docs/guides/cadvisor/
  prometheus:
    image: prom/prometheus:v2.28.0
    container_name: ${TEMPLATE}_prometheus
    ports:
      - 9090:9090
    command:
      - --config.file=/etc/prometheus/prometheus.yml
    volumes:
      - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml:ro
    networks:
      - NFinternal

  cadvisor:
    image: gcr.io/cadvisor/cadvisor:v0.39.0
    container_name: ${TEMPLATE}_cadvisor
    ports:
      - 8080:8080
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
    networks:
      - NFinternal
    depends_on:
      - zookeeper
      - kafka
      - nfipano
      - nfacctd
$( [ $TEMPLATE = 'development' ] && cat <<EOF2
      - pmacctd
EOF2
)
$( [ $TEMPLATE = 'nflowsim' ] && cat <<EOF2
      - nflow-generator
EOF2
)

  # thanks https://github.com/stefanprodan/dockprom/blob/master/docker-compose.yml
  grafana:
    image: grafana/grafana:8.0.3
    container_name: ${TEMPLATE}_grafana
    ports:
      - 3000:3000
    volumes:
      - data_grafana:/var/lib/grafana
      - ./grafana/provisioning/dashboards:/etc/grafana/provisioning/dashboards
      - ./grafana/provisioning/datasources:/etc/grafana/provisioning/datasources
    environment:
      - GF_SECURITY_ADMIN_USER=${ADMIN_USER:-admin}
      - GF_SECURITY_ADMIN_PASSWORD=${ADMIN_PASSWORD:-admin}
      - GF_USERS_ALLOW_SIGN_UP=false
    networks:
      - NFinternal

#  flink-jobmanager:
#    ## https://hub.docker.com/_/flink
#    image: flink:1.11.1
#    container_name: ${TEMPLATE}_flink-jobmanager
#    restart: unless-stopped
#    ports:
#      - "8081:8081"
#    logging:
#        driver: "json-file"
#        options:
#            max-file: "5"
#            max-size: "50m"
#    environment:
#      JOB_MANAGER_RPC_ADDRESS: flink-jobmanager
#    volumes:
#      - /etc/localtime:/etc/localtime:ro
#    networks:
#      - NFinternal
#    command: [ "jobmanager" ]

#  flink-taskmanager:
#    image: flink:1.11.1
#    container_name: ${TEMPLATE}_flink-taskmanager
#    restart: unless-stopped
#    logging:
#        driver: "json-file"
#        options:
#            max-file: "5"
#            max-size: "50m"
#    environment:
#      JOB_MANAGER_RPC_ADDRESS: flink-jobmanager
#    volumes:
#      - /etc/localtime:/etc/localtime:ro
#    networks:
#      - NFinternal
#    command: [ "taskmanager" ]


networks:
  internet:
    name: internet
    driver: bridge
    driver_opts:
      com.docker.network.bridge.name: "br-internet"
    internal: false
  NFinternal:
    name: NFinternal
    driver: bridge
    driver_opts:
      com.docker.network.bridge.name: "br-NFinternal"
$( if [ $TEMPLATE = 'nflowsim' ] || [ $TEMPLATE = 'development' ]; then cat <<EOF2
    internal: false
EOF2
else cat <<EOF2
    internal: true
EOF2
fi
)


volumes:
  data_zk_conf:
    name: data_zk_conf
  data_zk_data:
    name: data_zk_data
  data_zk_datalog:
    name: data_zk_datalog
  data_zk_logs:
    name: data_zk_logs
  data_kafka:
    name: data_kafka
  data_grafana:
    name: data_grafana

EOF
)"
