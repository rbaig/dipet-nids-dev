import json

from kafka import KafkaConsumer
from kafka import KafkaProducer

consumer = KafkaConsumer(
    'nfraw',
    bootstrap_servers=['kafka:19092'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    value_deserializer=lambda x: json.loads(x.decode('utf-8')))

producer = KafkaProducer(
    bootstrap_servers=['kafka:19092'],
    value_serializer=lambda x: json.dumps(x).encode('utf-8'))

# up to here LGTM

## Get first record
message = next(consumer)

## Build a tuple with existing primitives in the first record
IPaux = []
for i in IPs:
    if i in message.value:
    	IPaux.append(i)

IPc = tuple(IPaux)

## Anonymise firt message
for ip in IPc:
    if message.value[ip] not in ['', '0.0.0.0', '::']:
        message.value[ip] =  cp.anonymize(message.value[ip])

producer.send('nfano', value=message.value)

## Anonymise remaining messages
for message in consumer:
    for ip in IPc:
        if message.value[ip] not in ['', '0.0.0.0', '::']:
            message.value[ip] =  cp.anonymize(message.value[ip])
    producer.send('nfano', value=message.value)
