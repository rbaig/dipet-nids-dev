import json

from kafka import KafkaConsumer
from kafka import KafkaProducer

import secrets
from yacryptopan import CryptoPAn

## IPs to anonymise (as a tuple; tuples are more efficient than lists because are immutable)
IPs = ('ip_src', 'ip_dst', 'net_src', 'net_dst', 'peer_ip_src', 'peer_ip_dst', 'post_nat_ip_src', 'post_nat_ip_dst', 'tunnel_ip_src', 'tunnel_ip_dst')

## Encryption key ("range(32)" is the demo one)
#cp = CryptoPAn(bytes(range(32)))
cp = CryptoPAn(secrets.token_bytes(32))

consumer = KafkaConsumer(
    'nfraw',
    bootstrap_servers=['kafka:19092'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    value_deserializer=lambda x: json.loads(x.decode('utf-8')))

producer = KafkaProducer(
    bootstrap_servers=['kafka:19092'],
    value_serializer=lambda x: json.dumps(x).encode('utf-8'))

## Get first record
message = next(consumer)

## Build a tuple with existing primitives in the first record
IPaux = []
for i in IPs:
    if i in message.value:
    	IPaux.append(i)

IPc = tuple(IPaux)

## Anonymise firt message
for ip in IPc:
    if message.value[ip] not in ['', '0.0.0.0', '::']:
        message.value[ip] =  cp.anonymize(message.value[ip])

producer.send('nfano', value=message.value)

## Anonymise remaining messages
for message in consumer:
    for ip in IPc:
        if message.value[ip] not in ['', '0.0.0.0', '::']:
            message.value[ip] =  cp.anonymize(message.value[ip])
    producer.send('nfano', value=message.value)
