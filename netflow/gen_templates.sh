#!/bin/sh -e

prerun_checks() {
  RSYNC="$(command -v rsync || true)"

  if [ -z "${RSYNC}" ]; then
    echo "ERROR: this script needs, rsync, do \`apt install rsync\`"
    exit 1
  fi

  # ensure you are in netflow dir to generate the templates in the appropriate
  # directory
  if [ "$(dirname $0)" != '.' ]; then
    cd "$(dirname $0)"
  fi
}

gen_template() {
  TEMPLATE=$1
  . template/docker-compose.yml.sh
  ${RSYNC} --quiet --archive --delete template/  \
    --exclude 'docker-compose.yml.sh' \
    --exclude '*swp' \
    ${TEMPLATE}
  cat > ${TEMPLATE}/docker-compose.yml <<EOF
${DC_CONFIG}
EOF
}

main() {
  prerun_checks "$@"

  # load env vars and ensure non empty vars in templates
  set -u
  gen_template development
  gen_template production
  gen_template nflowsim
  set +u
}

main
